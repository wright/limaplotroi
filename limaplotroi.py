

"""
Called by plot_rois.mac from spec
""" 
import pylab as pl, sps, sys

specname, arrayname, roifile = sys.argv[1:4]
try:
    newcounter = sys.argv[4]
except:
    newcounter = "cnt" 

# Read data from spec suppressing the warning from numpy:
e = sys.stderr
sys.stderr = open("/dev/null","w")
arraydata = sps.getdata( specname, arrayname )
sys.stderr = e

# Read the ROI file:
lines = open(roifile).readlines()
# LIMA ROI F4M
# titles
camera = lines[0].split()[-1]
counters = []
rois = {}
for line in lines[2:]:
    items = line.split()
    counters.append( items[0] )
    rois[items[0]] = [int(x) for x in items[1:]]

def roi_to_box( roi ):
    typ, xmin, xmax, ymin, ymax = roi
    xbox = xmin, xmin, xmax, xmax, xmin
    ybox = ymin, ymax, ymax, ymin, ymin
    return xbox, ybox

roitype={0:'SUM', 1:'AVG', 2:'STD', 3:'MIN', 4:'MAX'}

    

# Now plot
fig = pl.figure()
pl.imshow(pl.log(arraydata), vmin=pl.log(90), 
          interpolation='nearest',
          aspect='equal',
          cmap=pl.cm.gray_r)
for cnt in counters:
    x,y = roi_to_box( rois[cnt] )
    pl.plot( x, y, "-", label="%s:%s"%(cnt,roitype[rois[cnt][0]]))
pl.title("%s %s %s"%(specname, arrayname, camera))
pl.xlim(0,arraydata.shape[1])
pl.ylim(0,arraydata.shape[0])
if len(counters)>0:
    pl.legend()
pl.xlabel("Zoom on a roi then press space")

def handle_press( evt ):
    ax = pl.gca()
    xmin,xmax = ax.get_xlim()
    ymin,ymax = ax.get_ylim()
    xmax = min(int(xmax), arraydata.shape[1]-1)
    xmin = max(int(xmin), 0)
    ymax = min(int(ymax), arraydata.shape[1]-1)
    ymin = max(int(ymin), 0)
    print "# Copy the one you want to spec:"
    for i in range(5):
        print roitype[i]," :  limaroiadd",camera,newcounter,i,xmin,xmax,ymin,ymax

    

fig.canvas.mpl_connect('key_press_event', handle_press)


pl.show()


